class TodosController < ApplicationController

  def index
    @todos = Todo.where({completed: false, user_id: current_user.id})
    @completes = Todo.where({completed: true, user_id: current_user.id})

  end

  def new
    @todo =  Todo.new
  end


  def create
    @todo = Todo.create({:name => params[:todo][:name], :completed => false, :user_id => current_user.id})

    if @todo.save
      redirect_to :action => 'index', :notice => "Your todo item was created!"
    else
      render "index"
    end

  end



  def show
  end

  def edit
    @todo = Todo.find(params[:id])
  end

  def update
    @todo = Todo.find(params[:id])

    if @todo.update_attribute(:completed, true)
      redirect_to :action => 'index', :notice => "Your todo was marked as completed!"
    else
      redirect_to :action => 'index', :notice => "Your todo was unable to be marked as completed!"
    end

  end

  def destroy
    @todo = Todo.find(params[:id])
    @todo.destroy

    redirect_to :action => 'index', :notice => "Your todo was deleted!"
  end


end
