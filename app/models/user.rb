class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :todos, dependent: :destroy

  has_many :roles, :through => :user_roles
  has_many :user_roles

  def admin?(role_sym)
    roles.any? { |r| r.name.underscore.to_sym == role_sym }
  end

  ROLES = %w[admin moderator author banned]



end
